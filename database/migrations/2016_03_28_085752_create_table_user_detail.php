<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUserDetail extends Migration
{
    protected $table = "user_detail";
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      if (!Schema::hasTable($this->table)) {

          Schema::create($this->table, function (Blueprint $table) {

            $table->engine = 'InnoDB';
            /** Primary key  */
            $table->increments('id');

            /** Main data  */
            $table->tinyInteger('gender')->default(0);
            $table->text('address')->nullable();
            $table->text('image')->nullable();
            $table->integer('user_id')->unsigned();

            /* Action */
            $table->nullableTimestamps();

            $table->foreign('user_id')->references('id')
            ->on('users')->onDelete('cascade')
            ->onUpdate('cascade');

          });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
