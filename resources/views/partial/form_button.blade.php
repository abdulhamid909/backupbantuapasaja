<div class="pull-right">
    <button type="reset" class="btn btn-primary">
    	<i class="fa fa-refresh" style="margin-right:5px"></i>Cancel
    </button>
    <button type="submit" class="btn btn-default">
    	<i class="fa fa-file" style="margin-right:5px"></i>Save
    </button>
    <button type="submit" class="btn btn-success" name="save_continue">
    	<i class="fa fa-check" style="margin-right:5px"></i>Save & Continue
    </button>
</div>