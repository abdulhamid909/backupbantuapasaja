<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta http-equiv="content-language" content="en" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="index,nofollow" />
<meta name="copyright" content="Copyright © 2015" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="google" content="notranslate" />

<title> @yield('title') - MediaKulo.id</title>
<meta name="description" content="@yield('meta-description')" />
<meta name="keywords" content="#" />
<meta name="author" content="Tlab Amazing">
<meta name="csrf-token" content="{{ csrf_token() }}" />
<link rel="canonical" href="http://MediaKulo.id" />

<link rel="apple-touch-icon" sizes="57x57" href="{{ URL::to('dist/images/fav/apple-icon-57x57.png') }}">
<link rel="apple-touch-icon" sizes="60x60" href="{{ URL::to('dist/images/fav/apple-icon-60x60.png') }}">
<link rel="apple-touch-icon" sizes="72x72" href="{{ URL::to('dist/images/fav/apple-icon-72x72.png') }}">
<link rel="apple-touch-icon" sizes="76x76" href="{{ URL::to('dist/images/fav/apple-icon-76x76.png') }}">
<link rel="apple-touch-icon" sizes="114x114" href="{{ URL::to('dist/images/fav/apple-icon-114x114.png') }}">
<link rel="apple-touch-icon" sizes="120x120" href="{{ URL::to('dist/images/fav/apple-icon-120x120.png') }}">
<link rel="apple-touch-icon" sizes="144x144" href="{{ URL::to('dist/images/fav/apple-icon-144x144.png') }}">
<link rel="apple-touch-icon" sizes="152x152" href="{{ URL::to('dist/images/fav/apple-icon-152x152.png') }}">
<link rel="apple-touch-icon" sizes="180x180" href="{{ URL::to('dist/images/fav/apple-icon-180x180.png') }}">
<link rel="icon" type="image/png" sizes="192x192"  href="{{ URL::to('dist/images/fav/android-icon-192x192.png') }}">
<link rel="icon" type="image/png" sizes="32x32" href="{{ URL::to('dist/images/fav/favicon-32x32.png') }}">
<link rel="icon" type="image/png" sizes="96x96" href="{{ URL::to('dist/images/fav/favicon-96x96.png') }}">
<link rel="icon" type="image/png" sizes="16x16" href="{{ URL::to('dist/images/fav/favicon-16x16.png') }}">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">

<!-- Fonts -->
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300,400' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
<!-- CSS Libs -->
<link rel="stylesheet" type="text/css" href="{!! url('') !!}/dist/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="{!! url('') !!}/dist/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="{!! url('') !!}/dist/css/animate.min.css">
<link rel="stylesheet" type="text/css" href="{!! url('') !!}/dist/css/bootstrap-switch.min.css">
<link rel="stylesheet" type="text/css" href="{!! url('') !!}/dist/css/checkbox3.min.css">
<link rel="stylesheet" type="text/css" href="{!! url('') !!}/dist/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="{!! url('') !!}/dist/css/dataTables.bootstrap.css">
<link rel="stylesheet" type="text/css" href="{!! url('') !!}/dist/css/select2.min.css">
<!-- CSS App -->
<link rel="stylesheet" type="text/css" href="{!! url('') !!}/css/style.css">
<link rel="stylesheet" type="text/css" href="{!! url('') !!}/css/themes/flat-blue.css">