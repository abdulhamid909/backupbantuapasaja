<div class="side-menu sidebar-inverse">
    <nav class="navbar navbar-default" role="navigation">
        <div class="side-menu-container">
            <div class="navbar-header">
                <a class="navbar-brand" href="">
                    <div class="icon fa fa-paper-plane"></div>
                    <div class="title">Administrator</div>
                </a>
                <button type="button" class="navbar-expand-toggle pull-right visible-xs">
                    <i class="fa fa-times icon"></i>
                </button>
            </div>
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="{!! url('home') !!}">
                        <span class="icon fa fa-tachometer"></span><span class="title">Dashboard</span>
                    </a>
                </li>

                <li class="panel panel-default dropdown">
                    <a data-toggle="collapse" href="#dropdown-master">
                        <span class="icon fa fa-file-text-o"></span><span class="title">Master</span>
                    </a>
                    <!-- Dropdown level 1 -->
                    <div id="dropdown-master" class="panel-collapse collapse">
                        <div class="panel-body">
                            <ul class="nav navbar-nav">
                                <li><a href="{!! url('groups') !!}">Groups</a></li>
                                <li><a href="{!! url('users') !!}">Users</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <!-- Dropdown-->
                <li class="panel panel-default dropdown">
                    <a data-toggle="collapse" href="#component-example">
                        <span class="icon fa fa-cubes"></span><span class="title">Components</span>
                    </a>
                    <!-- Dropdown level 1 -->
                    <div id="component-example" class="panel-collapse collapse">
                        <div class="panel-body">
                            <ul class="nav navbar-nav">
                                <li><a href="{!! url('/') !!}">Pricing Table</a>
                                </li>
                                <li><a href="{!! url('/') !!}">Chart.JS</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
                <!-- Dropdown-->
                <li class="panel panel-default dropdown">
                    <a data-toggle="collapse" href="#dropdown-example">
                        <span class="icon fa fa-slack"></span><span class="title">Page Example</span>
                    </a>
                    <!-- Dropdown level 1 -->
                    <div id="dropdown-example" class="panel-collapse collapse">
                        <div class="panel-body">
                            <ul class="nav navbar-nav">
                                <li><a href="{!! url('/') !!}">Login</a>
                                </li>
                                <li><a href="{!! url('/') !!}">Landing Page</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
                <!-- Dropdown-->
                <li class="panel panel-default dropdown">
                    <a data-toggle="collapse" href="#dropdown-icon">
                        <span class="icon fa fa-archive"></span><span class="title">Icons</span>
                    </a>
                    <!-- Dropdown level 1 -->
                    <div id="dropdown-icon" class="panel-collapse collapse">
                        <div class="panel-body">
                            <ul class="nav navbar-nav">
                                <li><a href="icons/glyphicons.html">Glyphicons</a>
                                </li>
                                <li><a href="icons/font-awesome.html">Font Awesomes</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li>
                    <a href="license.html">
                        <span class="icon fa fa-thumbs-o-up"></span><span class="title">License</span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>
</div>